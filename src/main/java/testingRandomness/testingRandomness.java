package testingRandomness;
import java.lang.Math;
import org.apache.commons.*;
import org.apache.commons.math3.util.ArithmeticUtils;
import lib.Rvms;

import lib.*;
public class testingRandomness {

	//function init of the probability array
	public static void init_prob(double p[],int k){
		int i;
		for(i = 1; i<=k; i++){
			p[i-1] = (i/ArithmeticUtils.factorialDouble(i+1));
		}
	}
	
	//function init of bins
	public static void init_bins(int bins[],int k){
		for(int i = 0; i<k; i++){
			bins[i] = 0;
		}
	}
	
	//function init of the array of the value
	public static void init_values(double values[],int runs_up){
		
		for(int j = 0; j<runs_up; j++){
			values[j] = 0;
		}
	}
	
	//this function calculate the value of each runs_up
	public static double calculateChiSquadreStatistic(int bins[], int k, double p[], int n){
		double v = 0;
		double z;
		int i;
		for(i = 0; i < k; ++i){
			z = Math.pow((bins[i] - n*p[i]) ,2)/(n*p[i]);
			v = v + z;		
		}
		return v;
	}
	
		public static void main(String[] args) {
			
			//init parameters(k,n,runs_up)
			int k = 6;
			int runs_up = 256;
			int n = 14400;
			int countError = 0;
			
			System.out.println("initializer value: \n K:" + k + " \n runs_up: "+ runs_up + "\n n: "+ n + "\n");
			System.out.println("----------------------------------------------------------");
	
			//confident value
			double alfa = 0.05;
			double levelOfConfidence = (1-alfa)*100;
			System.out.println("the level of confidence is : " + levelOfConfidence+"%");
			
			
			
			//critic value
			Rvms icdfChi = new Rvms();
			double vLow = icdfChi.idfChiSquare(k-1, alfa/2);
			double vUp = icdfChi.idfChiSquare(k-1, 1-alfa/2);
			
			System.out.println("The critical Value are : \n vLow : " + vLow + "\n vUp : " +vUp);
			System.out.println("----------------------------------------------------------");
	
			//init variable
			int[] bins = new int[k];		
			double[] p = new double[k];
			double[] values = new double[runs_up];
			init_values(values,runs_up);
			init_prob(p, k);		
			
			Rng r = new Rng();
			long seed = 12345;
			r.putSeed(seed);
			
			int x,y,z;
			double rand_u,rand_t;
			for(x = 0; x< runs_up; x++){
				
				init_bins(bins, k);
				for(y = 0; y<n; y++){
					z=0;
					rand_u = r.random();
					rand_t = r.random();
					while(rand_t>rand_u){
						z++;
						rand_u = rand_t;
						rand_t = r.random();
					}
					if(z>(k-1)){
						z = k-1;
					}
					bins[z]++;
				}
				
				//after each runs_up we calculate the value of ChiSquareStatistic, and we will go to match the value
				//with the critic values
				double v = calculateChiSquadreStatistic(bins,k,p,n);
				values[x] = v;
				System.out.println("----------------------------------------------------------");
				System.out.println("Values as v: " + values[x]);
				System.out.println("----------------------------------------------------------");
	
			}
			
			//after all runs_up we calculate the number of error.
			for (double d : values) {
				if(d <vLow){
					System.out.println("d : " + d);
					countError = countError + 1;
				}else if(d > vLow && d > vUp){
					System.out.println("d : " + d);
					countError = countError + 1;
				}
	
			}
			System.out.println("Count Error : " + countError);
	
			
		}

}
